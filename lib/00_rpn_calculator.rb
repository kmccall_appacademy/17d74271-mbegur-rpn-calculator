class RPNCalculator
  def initialize
    @calculator = []
  end

  def push(num)
    @calculator << num
  end

  def plus
    raise "calculator is empty" if @calculator.length < 2
    @calculator << (@calculator.pop + @calculator.pop)
  end

  def minus
    raise "calculator is empty" if @calculator.length < 2
    second_num = @calculator.pop
    first_num = @calculator.pop
    @calculator << (first_num - second_num)
  end

  def times
    raise "calculator is empty" if @calculator.length < 2
    @calculator << (@calculator.pop * @calculator.pop)
  end

  def divide
    raise "calculator is empty" if @calculator.length < 2
    second_num = @calculator.pop
    first_num = @calculator.pop
    @calculator << (first_num / second_num.to_f)
  end

  def value
    @calculator[-1]
  end

  def tokens(str)
    arr = str.split
    arr.map do |ch|
      if ["+", "-", "*", "/"].include?(ch)
        ch.to_sym
      else
        ch.to_i
      end
    end
  end

  def evaluate(str)
    token = tokens(str)
    token.each do |char|
      if char == :+
        plus
      elsif char == :-
        minus
      elsif char == :*
        times
      elsif char == :/
        divide
      else
        push(char)
      end
    end
    value
  end

end
